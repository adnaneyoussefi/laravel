@extends('layouts.app')


@section('content')
<div>
    
    <h1>Liste des participants</h1>
    <div class="flex-center">
    <div class="content">
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">Nom</th>
            <th scope="col">Prenom</th>
            <th scope="col">Email</th>
            <th scope="col">Atelier</th>
          </tr>
        </thead>
        <tbody>
    @foreach($participations as $participation)
    
          <tr class="table-active">
            <td class="text-black">{{$participation->nom}}</td>
            <td class="text-black">{{$participation->prenom}}</td>
            <td class="text-black">{{$participation->email}}</td>
            <td class="text-black">{{$participation->atelier}}</td>
          </tr>
          @endforeach
        </tbody>
    </table>
    </div>
    </div>
</div>
@endsection