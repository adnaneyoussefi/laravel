@extends('layouts.app')

@section('content')
    <div>
        <h1>Ajouter un participant</h1>
            <form method="post" action="{{route('addparticipant')}}">

            {{csrf_field()}}

            <strong><label for="nom">Nom</label></strong>
            <input type="text" name="nom" id="nom">

            <strong><label for="prenom">Prenom</label></strong>
            <input type="text" name="prenom" id="prenom">

            <strong><label for="email">Email</label></strong>
            <input type="email" name="email" id="email">

            <strong><label for="atelier">Atelier</label></strong>
            <select name="atelier" id="atelier">
                <option value="Atelier1">Atelier1</option>
                <option value="Atelier2">Atelier2</option>
                <option value="Atelier3">Atelier3</option>
            </select>

            <button class="btn btn-primary" type="submit">Ajouter</button>
            </form>
    </div>
@endsection