<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Participation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function ajouter() {
        return view('ajouter');
    }

    public function afficher() {
        $participations = Participation::all();
        return view('afficher',[
            'participations' => $participations,
        ]);
    }

    public function add(Request $req){
        $param = $req->all();
        
        $participation = new Participation();
        $participation->nom = $param['nom'];
        $participation->prenom = $param['prenom'];
        $participation->email = $param['email'];
        $participation->atelier = $param['atelier'];
        $participation->save();
        return redirect()->route('afficher');
    }
}
