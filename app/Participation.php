<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participation extends Model
{
    protected $table = 'participation';
    protected $fillable = ['nom','prenom','email','atelier'];
}
